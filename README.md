# tag

Component for tag in general.

## Installation

```yml
# zillean-domain.yml

name: domain
comps:
 - https://gitlab.com/zilleanai/comp/tag
```

```py
# unchained_config.py

BUNDLES = [
    'flask_unchained.bundles.api',
...
    'bundles.project',
    'bundles.tag',
...
    'backend',  # your app bundle *must* be last
]
```

```py
# routes.py

routes = lambda: [
    include('bundles.project.routes'),
...
    include('bundles.tag.routes'),
...
    controller(SiteController), 
]
```

```js
// routes.js
import {
  Tags
} from 'comps/tag/pages'
...
export const ROUTES = {
  Home: 'Home',
  ...
  Tags: 'Tags',
  ...
}
...
const routes = [
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  ...
  {
    key: ROUTES.Tags,
    path: '/Tags/:subpath',
    component: Tags,
  },
  ...
]
```

```js
// NavBar.js
<div className="menu left">
    <NavLink to={ROUTES.Projects} />
    ...
    <NavLink to={ROUTES.Tags} params={{subpath:'/'}}/>
    ...
</div>
```
