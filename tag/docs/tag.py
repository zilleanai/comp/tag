import os


class tag():

    def index(self):
        index_file = os.path.join(os.path.dirname(
            os.path.abspath(__file__)), 'index.md')
        return index_file

    def path(self, path):
        file_path = os.path.join(os.path.dirname(
            os.path.abspath(__file__)), path)
        if os.path.exists(file_path):
            return file_path
        return None
