from flask_unchained.bundles.api import ma

from ..models import Tag

TAG_FIELDS = ('id', 'name')


class TagSerializer(ma.ModelSerializer):
    class Meta:
        model = Tag
        fields = TAG_FIELDS


@ma.serializer(many=True)
class TagListSerializer(TagSerializer):
    class Meta:
        model = Tag
        fields = TAG_FIELDS
