from flask import request
from flask_unchained.bundles.api import ModelResource
from flask_unchained import injectable

from ..models import Tag
from ..services import TagManager

class TagResource(ModelResource):
    class Meta:
        model = Tag
        include_methods = ('create', 'get', 'list')
        include_decorators = ('get', 'list')
        member_param = '<string:name>'

    def __init__(self,
                 tag_manager: TagManager = injectable):
        super().__init__()
        self.tag_manager = tag_manager

    def create(self):
        name = request.json['name']
        print(name)
        tag = self.tag_manager.create(name=name)
        return self.created(tag)