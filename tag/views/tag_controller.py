import os
import json
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from io import StringIO, BytesIO
from werkzeug.utils import secure_filename

from ..db import TagDB


class TagController(Controller):

    @route('/tag/<string:project>', methods=['POST'])
    def create(self, project):
        name = request.json['name']
        TagDB(project).addTag(name)
        return jsonify(success=True)

    @route('/tags/<string:project>', methods=['GET'])
    def list_tags(self, project):
        tags = TagDB(project).listTags()
        return jsonify(project=project, tags=tags)

    @route('/tags/<string:project>/<string:tag>', methods=['GET'])
    def tag(self, project, tag):
        ret_tag = TagDB(project).getTag(name=tag)
        if ret_tag:
            return jsonify(project=project, name=tag)
        return jsonify(abort(404))
