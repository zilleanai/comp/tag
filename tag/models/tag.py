from flask_unchained.bundles.sqlalchemy import db


class Tag(db.Model):
    name = db.Column(db.String(32))

    __repr_props__ = ('id', 'name')
