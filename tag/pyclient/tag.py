import requests
from io import BytesIO


class tag():
    def __init__(self, project={'api_root': ''}):
        self.project = project

    def list(self):
        url = self.project['api_root'] + 'tag/tags/' + \
            str(self.project['name'])
        return requests.get(url).json()['tags']
