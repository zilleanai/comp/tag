import os
from backend.config import Config as AppConfig

import sqlite3


class TagDB():
    def __init__(self, project):
        BASE_DIR = os.path.join(
            AppConfig.DATA_FOLDER, project)
        self.project = project
        self.conn = sqlite3.connect(os.path.join(BASE_DIR, 'database.db'))
        self.createTables()

    def createTables(self):
        c = self.conn.cursor()
        c.execute('''CREATE TABLE IF NOT EXISTS `tag` (name text PRIMARY KEY)''')
        self.conn.commit()

    def addTag(self, name):
        c = self.conn.cursor()
        c.execute('INSERT OR IGNORE INTO `tag` VALUES (?)', (name,))
        self.conn.commit()

    def getTag(self, name):
        c = self.conn.cursor()
        rows = c.execute(
            '''SELECT * FROM `tag` WHERE name=?''', (name,)).fetchall()
        for row in rows:
            return row
        return None

    def listTags(self):
        c = self.conn.cursor()
        rows = c.execute('''SELECT * FROM `tag`''')
        tags = [ix[0] for ix in rows]
        return tags
