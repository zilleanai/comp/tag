import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'

import { storage } from 'comps/project'
import { listTags } from 'comps/tag/actions'
import { selectTagsList } from 'comps/tag/reducers/tags'
import Select from 'react-select';

import './tag-select.scss'

class TagSelect extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedOption: null,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    if (this.props.onChange) {
      this.props.onChange({ value: this.optionsToItems(selectedOption) })
    }
  }

  componentWillMount() {
    this.props.listTags.trigger()
  }

  itemsToOptions(items) {
    var options = []
    items.forEach(function (value, key) {
      options.push({ value: value, label: value })
    }, items)
    return options
  }

  optionsToItems(options) {
    if(!options){
      return []
    }
    var items = []
    options.forEach(function (value, key) {
      items.push(value.value)
    }, options)
    return items
  }

  selectedOptions(selected, options) {
    var selOptions = []
    if (!!selected && !!options) {
      options.forEach(function (value, key) {
        if (selected.indexOf(value.value) > -1) {
          selOptions.push(value)
        }
      }, options)
    }
    return selOptions
  }

  render() {
    const { isLoaded, error, tags, selected, defaultValue } = this.props
    if (!isLoaded) {
      return null
    }
    const options = this.itemsToOptions(tags)
    const dvalue = this.selectedOptions(defaultValue, this.itemsToOptions(tags))
    return (
      <div className='tag-select'>
        <Select
          value={this.state.selectedOption || dvalue || null}
          onChange={this.handleChange}
          isSearchable={true}
          isClearable={true}
          name="tags"
          options={options}
          isMulti
          className="basic-multi-select"
          classNamePrefix="select"
        />
      </div>
    )
  }
}

const withReducer = injectReducer(require('comps/tag/reducers/tags'))

const withSaga = injectSagas(require('comps/tag/sagas/tags'))

const withConnect = connect(
  (state, props) => {
    const selected = props.selected
    const defaultValue = props.defaultValue
    const tags = selectTagsList(state, storage.getProject())
    const isLoaded = !!tags
    return {
      tags,
      selected,
      defaultValue,
      isLoaded
    }
  },
  (dispatch) => bindRoutineCreators({ listTags }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(TagSelect)
