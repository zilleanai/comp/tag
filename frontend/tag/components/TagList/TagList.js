import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'

import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { storage } from 'comps/project'
import { listTags } from 'comps/tag/actions'
import { selectTagsList } from 'comps/tag/reducers/tags'

import TagLink from '../TagLink'

import './tag-list.scss'


class TagList extends React.Component {
  componentWillMount() {
    const { listTags } = this.props
    listTags.maybeTrigger()
  }

  render() {
    const { current, tags } = this.props
    if (!tags) {
      return null
    }
    return (
      <ul className="tag-list">
        {tags.map((tag, i) => {
          const active = current && tag == current
          return (
            <li key={i} className={classnames({ active })}>
              <span>
                <TagLink tag={tag} active={active} />
              </span>
            </li>
          )
        })}
      </ul>
    )
  }
}

const withReducer = injectReducer(require('comps/tag/reducers/tags'))

const withSagas = injectSagas(require('comps/tag/sagas/tags'))

const withConnect = connect(
  (state) => ({ tags: selectTagsList(state, storage.getProject()) }),
  (dispatch) => bindRoutineCreators({ listTags }, dispatch),
)

export default compose(
  withReducer,
  withSagas,
  withConnect,
)(TagList)
