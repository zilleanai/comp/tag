import React from 'react'

import { NavLink } from 'components'
import { ROUTES } from 'routes'


export default ({ active, tag }) => active
  ? <strong>{tag}</strong>
  : <NavLink to={ROUTES.TagDetail} params={{name:tag}}>
      {tag}
    </NavLink>
