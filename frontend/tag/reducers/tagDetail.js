import { loadTagDetail } from 'comps/tag/actions'


export const KEY = 'tagDetail'

const initialState = {
  isLoading: false,
  ids: [],
  byId: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { tag } = payload || {}
  const { ids, byId } = state

  switch (type) {
    case loadTagDetail.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case loadTagDetail.SUCCESS:
      const id = tag.project + tag.name
      if (!ids.includes(id)) {
        ids.push(id)
      }
      byId[id] = tag
      return {
        ...state,
        ids,
        byId,
      }

    case loadTagDetail.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectTagDetail = (state) => state[KEY]
export const selectTagByName = (state, project, name) => selectTagDetail(state).byId[project + name]
