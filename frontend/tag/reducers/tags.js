import { listTags } from 'comps/tag/actions'


export const KEY = 'tags'

const initialState = {
  isLoading: false,
  isLoaded: false,
  projects: [],
  byProject: {},
  error: null,
}

export default function(state = initialState, action) {
  const { type, payload } = action
  const { tags } = payload || {}
  const { projects, byProject } = state
  switch (type) {
    case listTags.REQUEST:
      return { ...state,
        isLoading: true,
      }

    case listTags.SUCCESS:
    if (!projects.includes(tags.project)) {
      projects.push(tags.project)
    }
    byProject[tags.project] = tags.tags
    return {
      ...state,
      projects,
      byProject,
    }

    case listTags.FULFILL:
      return { ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectTags = (state) => state[KEY]
export const selectTagsList = (state, project) => {
  const tags = selectTags(state).byProject[project]
  return tags
}
