import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'

import { listTags } from 'comps/tag/actions'
import TagApi from 'comps/tag/api'
import { selectTags } from 'comps/tag/reducers/tags'


export const KEY = 'tags'

export const maybeListTagsSaga = function *() {
  const { isLoading, isLoaded } = yield select(selectTags)
  if (!(isLoaded || isLoading)) {
    yield put(listTags.trigger())
  }
}

export const listTagsSaga = createRoutineSaga(
  listTags,
  function *successGenerator() {
    const tags = yield call(TagApi.listTags)
    yield put(listTags.success({ tags }))
  }
)

export default () => [
  takeEvery(listTags.MAYBE_TRIGGER, maybeListTagsSaga),
  takeLatest(listTags.TRIGGER, listTagsSaga),
]
