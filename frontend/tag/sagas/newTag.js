import { call, put, takeLatest } from 'redux-saga/effects'

import { newTag } from 'comps/tag/actions'
import { createRoutineFormSaga } from 'sagas'
import TagApi from 'comps/tag/api'


export const KEY = 'new_tag'

export const newTagSaga = createRoutineFormSaga(
  newTag,
  function* successGenerator(payload) {
    const response = yield call(TagApi.newTag, payload)
    yield put(newTag.success(response))
  }
)

export default () => [
  takeLatest(newTag.TRIGGER, newTagSaga)
]
