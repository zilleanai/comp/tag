import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { loadTagDetail } from 'comps/tag/actions'
import TagApi from 'comps/tag/api'
import { selectTagDetail } from 'comps/tag/reducers/tagDetail'


export const KEY = 'tagDetail'

export const maybeLoadTagDetailSaga = function *(tag) {
  const { byId, isLoading } = yield select(selectTagDetail)
  const isLoaded = !!byId[tag.project+tag.name]
  if (!(isLoaded || isLoading)) {
    yield put(loadTagDetail.trigger(tag))
  }
}

export const loadTagDetailSaga = createRoutineSaga(
  loadTagDetail,
  function *successGenerator({ payload: tag }) {
    var tag = yield call(TagApi.loadTagDetail, tag)
    yield put(loadTagDetail.success({ tag }))
  }
)

export default () => [
  takeEvery(loadTagDetail.MAYBE_TRIGGER, maybeLoadTagDetailSaga),
  takeLatest(loadTagDetail.TRIGGER, loadTagDetailSaga),
]
