import { get, post } from 'utils/request'
import { v1 } from 'api'
import { storage } from 'comps/project'

function tag(uri) {
  return v1(`/tag${uri}`)
}

export default class Tag {

  static listTags() {
    return get(tag(`/tags/${storage.getProject()}`))
  }

  /**
   * @param {Object} tag
   * @param {string} tag.name
   */
  static loadTagDetail(tag_) {
    return get(tag(`/tags/${storage.getProject()}/${tag_.name}`))
  }

  static newTag({ name }) {
    return post(tag(`/tag/${storage.getProject()}`), { 'name': name })
  }
}
