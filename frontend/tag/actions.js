import { createRoutine } from 'actions'

export const listTags = createRoutine('tag/LIST_TAGS')
export const loadTagDetail = createRoutine('tag/LOAD_TAG_DETAIL')
export const newTag = createRoutine('tag/NEW_TAG')