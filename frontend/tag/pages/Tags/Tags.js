import React from 'react'
import Helmet from 'react-helmet'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { Field, reduxForm, formValues } from 'redux-form'
import formActions from 'redux-form/es/actions'
const { reset } = formActions
import { bindRoutineCreators } from 'actions'
import { PageContent } from 'components'
import { Steps } from 'intro.js-react';
import 'intro.js/introjs.css';
import { injectReducer, injectSagas } from 'utils/async'

import { TagList, TagSelect } from 'comps/tag/components'
import { newTag, listTags } from 'comps/tag/actions'
import { TextField } from 'components/Form'
const FORM_NAME = 'newTag'

class Tags extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      stepsEnabled: true,
      initialStep: 0,
      steps: [
        {
          element: '.tag-page',
          intro: 'Tags can be created and viewed on this page. Tags will be used for semantic of data and files on some pages.',
        },
        {
          element: '.tag-name',
          intro: 'Name of a new tag.',
        },
        {
          element: '.button-primary',
          intro: 'Create a new tag here.',
        },
        {
          element: '.tag-list',
          intro: 'List of tags to view.',
        },
      ],
    };
  }

  render() {
    const { error, handleSubmit, pristine, submitting } = this.props
    const { stepsEnabled, steps, initialStep } = this.state;
    return (
      <PageContent className='tag-page'>
        <Steps
          enabled={stepsEnabled}
          steps={steps}
          initialStep={initialStep}
          onExit={() => {
            this.setState(() => ({ stepsEnabled: false }));
          }}
        />
        <Helmet>
          <title>Tags</title>
        </Helmet>
        <h1>Tags</h1>
        <form onSubmit={handleSubmit(newTag)}>
          <TextField className='tag-name' name='name' />
          <div className="row">
            <button type="submit"
              className="button-primary"
              disabled={pristine || submitting}
            >
              {submitting ? 'New...' : 'New'}
            </button>
          </div>
        </form>
        <TagList />
      </PageContent>
    )
  }
}

const withConnect = connect(
  (state) => {
    const isLoaded = true
    if (isLoaded) {
      return {
        isLoaded
      }
    } else {
      return {
        isLoaded: false
      }
    }
  },
  (dispatch) => bindRoutineCreators({}, dispatch),
)

const withForm = reduxForm({
  form: FORM_NAME,
  onSubmitSuccess: (_, dispatch) => {
    dispatch(listTags.trigger())
    dispatch(reset(FORM_NAME))
  }
})

const withSaga = injectSagas(require('comps/tag/sagas/newTag'))

export default compose(
  withSaga,
  withForm,
  withConnect,
)(Tags)
