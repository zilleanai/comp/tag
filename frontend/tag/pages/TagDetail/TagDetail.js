import React from 'react'
import Helmet from 'react-helmet'
import { compose } from 'redux'
import { connect } from 'react-redux'

import { bindRoutineCreators } from 'actions'
import { PageContent } from 'components'
import { injectReducer, injectSagas } from 'utils/async'
import { storage } from 'comps/project'
import { loadTagDetail } from 'comps/tag/actions'
import { selectTagByName } from 'comps/tag/reducers/tagDetail'
import { TagList } from 'comps/tag/components'


class TagDetail extends React.Component {
  componentWillMount() {
    const { loadTagDetail, name } = this.props
    loadTagDetail.maybeTrigger({ name })
  }

  componentWillReceiveProps(nextProps) {
    const { loadTagDetail, name } = nextProps
    if (name != this.props.name) {
      loadTagDetail.maybeTrigger({ name })
    }
  }

  render() {
    const { isLoaded, tag } = this.props
    if (!isLoaded) {
      return null
    }

    const { name } = tag

    return (
      <PageContent>
          <Helmet>
            <title>Tag: {name}</title>
          </Helmet>
          <h1>Tag: {name}</h1>
          <TagList />
      </PageContent>
    )
  }
}

const withReducer = injectReducer(require('comps/tag/reducers/tagDetail'))
const withSagas = injectSagas(require('comps/tag/sagas/tagDetail'))

const withConnect = connect(
  (state, props) => {
    const name = props.match.params.name
    const project = storage.getProject()
    const tag = selectTagByName(state, project, name)
    return {
      name,
      tag,
      isLoaded: !!tag,
    }
  },
  (dispatch) => bindRoutineCreators({ loadTagDetail }, dispatch),
)

export default compose(
  withReducer,
  withSagas,
  withConnect,
)(TagDetail)
